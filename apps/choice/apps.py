from django.apps import AppConfig


class ChoiceConfig(AppConfig):
    name = 'apps.choice'
