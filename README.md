# Django Boilerplate
This app is designed to make the initialization of a new Django project simpler and quicker.


### Current Django Version
I typically only deploy using the latest LTS version of Django,
which is currently version 2.2.13.
At some point, this will be updated for 3.2 LTS.


#### Initialization Steps
 - Create `apps` directory
 - Create Custom User Auth Model extending AbstractUser (`apps.user`)
 - Create Choice App for managing CHOICES outside of Code (`apps.choice`)
 - Install Django Debug Toolbar & Django Debug Toolbar Request History and configure for use in DEBUG environments
 - Install Django Simple History
 - Create Environment-specific sections in `settings.py`
 - Create Empty `media`, `static`, and `templates` directories
 - Install gunicorn & whitenoise
 
 
#### Custom Management Commands
 - `createadminuser` quickly create admin user with password admin in DEBUG environments
 
 
#### To Do
 - Create Base Template using Bootstrap
 - Look for allowed hosts in PCF variables
 - Serializable Abstract Model
 - Common Template Tags
 - Common Abstracts
 - Extend `startapp` to automatically put new apps in `apps` directory (and creates static and templates folder with app name sub)
 - WSSO & Middleware - Deny Middleware
 - Create Style.MD to define structure (format, folders, choice, VerbNoun vs NounVerb, class names '-' vs '_', management commands naming)